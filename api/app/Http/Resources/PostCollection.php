<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'title' => $this->title,
			'slug' => $this->image,
			'image' => $this->image,
			'content' => $this->content,
			'created_at' => (string) $this->created_at,
			'updated_at' => (string) $this->updated_at,
			'user_id' => $this->user_id,
		];
    }
}
