<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
	public function register(Request $request)
    {		
		$errors = array();
		$user = User::where('email', '=', $request->email)->first();
		if ($user !== null) {
			$errors['email'] = array('The email has already been taken.');
		}
		
		if($request->password !== $request->password_confirmation){
			$errors['password'] = array('The password confirmation does not match.');
		}
		
		if(empty($request->name)){
			$errors['name'] = array('The name field is required.');
		}
		
		if(!empty($errors)){
			return response()->json([
				'message' => 'The given data was invalid.',
				'errors' => $errors
			], 422);
		}else{
			$user = User::create([
				'name' => $request->name,
				'email' => $request->email,
				'password' => bcrypt($request->password),
			]);
		
			$token = auth()->login($user);
		
			return $this->respondWithToken($token, $user);
		}
    }
	
	protected function respondWithToken($token, $user)
	{
		return response()->json([
			'name' => $user->name,
			'email' => $user->email,
			'updated_at' => $user->updated_at,
			'created_at' => $user->created_at,
			'id' => $user->id
		], 201);
	}
	
	public function login(Request $request)
    {
		$credentials = $request->only(['email', 'password']);
		$errors = array();
		
		
		if(empty($request->email)){
			$errors['email'] = array('The email field is required.');
		}
		if(empty($request->password)){
			$errors['password'] = array('The password field is required.');
		}
		
		if(!empty($errors)){
			return response()->json([
				'message' => 'The given data was invalid.',
				'errors' => $errors
			], 422);
		}else{
			if (!$token = auth()->attempt($credentials)) {
				$errors['email'] = array('These credentials do not match our records.');
				return response()->json([
					'message' => 'The given data was invalid.',
					'errors' => $errors
				], 422);
			}else{				
				return response()->json([
					'token' => $token,
					'token_type' => 'bearer',
					'expires_at' => Carbon::now()->addSeconds(auth()->factory()->getTTL() * 60)->toDateTimeString()
				], 200);
			}
		}
	}
	
	public function logout(Request $request)
    {
		$token = $request->header( 'Authorization' );
		JWTAuth::parseToken()->invalidate( $token );
		return response()->json( [
			'error'   => false,
			'message' => trans( 'auth.logged_out' )
		] );

	}
}
