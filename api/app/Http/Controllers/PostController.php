<?php

namespace App\Http\Controllers;

use App\Post;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;

class PostController extends Controller
{
	
	public function __construct()
    {
      $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		return PostResource::collection(Post::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$errors = array();
		if(empty($request->title)){
			$errors['content'] = array('The content field is required.');
		}
		if(empty($request->content)){
			$errors['title'] = array('The title field is required.');
		}
		if(!empty($errors)){
			return response()->json([
				'message' => 'The given data was invalid.',
				'errors' => $errors
			], 422);
		}else{
			$post = Post::create([
				'title' => $request->title,
				'content' => $request->content,
				'image' => $request->image,
				'user_id' => $request->user()->id,
				'deleted_at' => null
			]);
			
			return new PostResource($post);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
		//try{
			return new PostResource($post);
			//return new PostResource(Post::find($post->id));
		//}catch (Throwable $e) {
		//	return response()->json([
		//		'message' => 'No query results for model [App\\Post].'
		//	], 404);
		//}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
	  
      $post->update($request->only(['title', 'content', 'image']));

      return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
		$book->delete();

		return response()->json([
			'status' => 'record deleted successfully'
		], 200);
    }
}
