<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $fillable = [
		'title', 'body', 'commentable_type',
		'commentable_id', 'creator_id', 'parent_id'
	];
    //
	public function post(){
		return $this->belongsTo(Post::class);
	}
}
